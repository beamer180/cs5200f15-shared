﻿using System;
using System.Collections;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using log4net;

namespace Utils
{
    public class State
    {
        #region Private Data Members
        private static readonly ILog log = LogManager.GetLogger(typeof(State));
        protected object myLock = new object();
        #endregion

        #region Public Events
        public event StateChange.Handler StateChanged;
        public event StateChange.Handler ErrorsChanged;
        public event StateChange.Handler Shutdown;
        #endregion

        #region Status Stuff
        public virtual string StatusString { get { return string.Empty; } }
        #endregion

        #region Public Properties and Methods

        public bool Quit { get; set; }
       
        #endregion

        #region Event Raising Methods
        public void RaiseStateChangedEvent()
        {
            if (StateChanged != null)
            {
                log.Debug("Raise StateChanged event");
                StateChanged();
                log.Debug("Back from raising StateChanged event");
            }
        }

        public void RaiseErrorsChangedEvent()
        {
            if (ErrorsChanged != null)
            {
                log.Debug("Raise ErrorsChanged event");
                List<Error> errorCopy = new List<Error>();
                lock (myLock)
                {
                    foreach (Error error in myErrors)
                        errorCopy.Add(error);
                }
                ErrorsChanged();
                log.Debug("Back from raising ErrorChanged event");
            }
        }

        public void RaiseShutdownEvent()
        {
            log.Debug("Enter RaiseShutdownEvent");
            if (Shutdown != null)
            {
                log.Debug("Raise Shutdown event");
                Shutdown();
                log.Debug("Back from raising Shutdown event");
            }
            else
                log.Debug("Nobody is registered for the shutdown");
            log.Debug("Leave RaiseShutdownEvent");
        }

        #endregion

        #region Error Management Methods
        private List<Error> myErrors = new List<Error>();
        private const int MAX_ERRORS_TO_KEEP = 10;

        public Error[] Errors
        {
            get
            {
                Error[] result = new Error[myErrors.Count];
                myErrors.CopyTo(result);
                return result;
            }
        }

        public void AddError(Error err)
        {
            if (err != null)
            {
                log.WarnFormat("Error occurred, adding error to state: {0}, {1}", err.Number, err.Message);
                lock (myLock)
                {
                    if (myErrors.Count > MAX_ERRORS_TO_KEEP)
                        myErrors.RemoveRange(0, myErrors.Count - MAX_ERRORS_TO_KEEP);
                    myErrors.Add(err);
                    RaiseErrorsChangedEvent();
                }
            }
        }
        #endregion
    }
}
