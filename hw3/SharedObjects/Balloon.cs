﻿using System;
using System.Runtime.Serialization;

namespace SharedObjects
{
    [DataContract]
    public class Balloon : SharedResource
    {
        [DataMember]
        public bool IsFilled { get; set; }
    }
}
