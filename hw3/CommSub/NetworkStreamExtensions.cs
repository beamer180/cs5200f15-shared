﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

using System.Net;
using System.Net.Sockets;
using System.IO;

using Messages.StreamMessages;
using SharedObjects;

using log4net;

namespace CommSub
{
    public static class NetworkStreamExtensions
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(NetworkStreamExtensions));

        public static StreamMessage ReadStreamMessage(this NetworkStream stream)
        {
            log.DebugFormat("In ReadStreamMessage, with stream.ReadTimeout={0}", (stream==null) ? "null" : stream.ReadTimeout.ToString());

            StreamMessage result = null;

            byte[] bytes = new byte[4];

            int bytesRead = 0;
            try
            {
                log.DebugFormat("Try to read {0} length bytes, with stream.CanRead={1}", bytes.Length, stream.CanRead);
                while (stream.CanRead && bytesRead < bytes.Length)
                    bytesRead += stream.Read(bytes, 0, bytes.Length);
            }
            catch (IOException) { }

            log.DebugFormat("Length bytes read = {0}", bytesRead);

            if (bytesRead == bytes.Length)
            {
                int messageLength = IPAddress.NetworkToHostOrder(BitConverter.ToInt32(bytes, 0));
                log.DebugFormat("Incoming message will be {0} bytes", messageLength);

                bytes = new byte[messageLength];

                bytesRead = 0;
                try
                {
                    log.DebugFormat("Try to read {0} data bytes, with stream.CanRead={1}", bytes.Length, stream.CanRead);
                    while (stream.CanRead && bytesRead < bytes.Length)
                        bytesRead += stream.Read(bytes, bytesRead, bytes.Length - bytesRead);
                }
                catch (IOException)
                {
                    throw new ApplicationException(string.Format("Expected {0} bytes of messsage data, but it did not arrive within {1}", messageLength, stream.ReadTimeout));
                }

                log.DebugFormat("Read {0} bytes", bytesRead);

                if (bytesRead == bytes.Length)
                    result = StreamMessage.Decode(bytes);
            }
            return result;
        }

        public static void WriteStreamMessage(this NetworkStream stream, StreamMessage message)
        {
            log.DebugFormat("In WriteStreamMessage, message={0}", (message == null) ? "null" : message.GetType().ToString());
            if (stream != null && message != null)
            {
                byte[] messageBytes = message.Encode();
                byte[] lengthBytes = BitConverter.GetBytes(IPAddress.HostToNetworkOrder(messageBytes.Length));
                if (stream.CanWrite)
                {
                    try
                    {
                        stream.Write(lengthBytes, 0, lengthBytes.Length);
                        stream.Write(messageBytes, 0, messageBytes.Length);
                        log.Debug("Write complete");
                    }
                    catch (Exception err)
                    {
                        log.Error(err.Message);
                    }
                }
                else
                    log.Warn("Stream is not writable");
            }
        }
    }
}
