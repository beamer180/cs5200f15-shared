﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using System.Net;
using log4net;

using SharedObjects;
using Utils;

namespace CommSub
{
    public class ProcessAddressBook
    {
        #region Private Data Members
        private static readonly ILog log = LogManager.GetLogger(typeof(ProcessAddressBook));

        private Dictionary<Int32, ProcessInfo> processes = new Dictionary<Int32, ProcessInfo>();
        private Dictionary<string, Int32> endpoints = new Dictionary<string, Int32>();
        private object myLock = new object();
        #endregion

        public Error Add(ProcessInfo processInfo)
        {
            Error error = null;
            if (processInfo==null)
                error = Error.Get(Error.StandardErrorNumbers.InvalidProcessInformation);
            else if (processInfo.ProcessId < 0)
                error = Error.Get(Error.StandardErrorNumbers.InvalidProcessId);
            else if (processInfo.EndPoint == null)
                error = Error.Get(Error.StandardErrorNumbers.InvalidProcessInformation);
            else
            {
                Remove(processInfo.ProcessId, false);
                Remove(processInfo.EndPoint, false);

                log.DebugFormat("Add ({0}, {1}) to ProcessAddressBook", processInfo.ProcessId, processInfo.EndPoint.ToString());
                lock (myLock)
                {
                    processes.Add(processInfo.ProcessId, processInfo);
                    endpoints.Add(processInfo.EndPoint.ToString(), processInfo.ProcessId);
                }
            }
            return error;
        }

        public Error Add(Int32 processId, PublicEndPoint ep)
        {
            ProcessInfo info = new ProcessInfo() { ProcessId = processId, EndPoint = ep };
            return Add(info);
        }

        public Error UpdateProcess(ProcessInfo remoteProcess)
        {
            log.DebugFormat("Enter UpdateProcss, with ProcessId={0}", remoteProcess.ProcessId);

            Error error = null;
            if (remoteProcess.ProcessId < 0)
                error = Error.Get(Error.StandardErrorNumbers.InvalidProcessId);
            else
            {
                lock (myLock)
                {
                    if (processes.ContainsKey(remoteProcess.ProcessId))
                    {
                        string epString = processes[remoteProcess.ProcessId].EndPoint.ToString();
                        if (endpoints.ContainsKey(epString))
                            endpoints.Remove(epString);
                        endpoints.Add(epString, remoteProcess.ProcessId);
                        processes[remoteProcess.ProcessId] = remoteProcess;
                        log.DebugFormat("Process {0} logout", remoteProcess.ProcessId);
                    }
                    else
                        error = Error.Get(Error.StandardErrorNumbers.UnknownProcessId);
                }
            }

            log.Debug("Leave UpdateProcss");
            return error;
        }

        public Error Remove(Int32 processId, bool errorIfUnknown = true)
        {
            Error error=null;
            if (processId < 0)
                error = Error.Get(Error.StandardErrorNumbers.InvalidProcessId);
            else
            {
                lock (myLock)
                {
                    if (processes.ContainsKey(processId))
                    {
                        log.DebugFormat("Remove Id={0} from ProcessAddressBook", processId);
                        endpoints.Remove(processes[processId].ToString());
                        processes.Remove(processId);
                    }
                    else if (errorIfUnknown)
                        error = Error.Get(Error.StandardErrorNumbers.UnknownProcessId);
                }
            } 
            return error;
        }

        public Error Remove(PublicEndPoint ep, bool errorIfUnknown = true)
        {
            Error error = null;
            if (ep == null)
                error = Error.Get(Error.StandardErrorNumbers.EndPointCannotBeNull);
            else
            {
                lock (myLock)
                {
                    if (endpoints.ContainsKey(ep.ToString()) && endpoints[ep.ToString()] > 0)
                    {
                        log.DebugFormat("Remove ep={0} from ProcessAddressBook", ep.ToString());
                        processes.Remove(endpoints[ep.ToString()]);
                        endpoints.Remove(ep.ToString());
                    }
                    else if (errorIfUnknown)
                        error = Error.Get(Error.StandardErrorNumbers.UnknownEndPoint);
                }
            }
            return error;
        }

        public ProcessInfo this[Int32 processId]
        {
            get
            {
                ProcessInfo result = null;
                lock (myLock)
                {
                    if (processes.ContainsKey(processId))
                        result = processes[processId];
                }
                return result;
            }
        }

        public Int32 this[PublicEndPoint ep]
        {
            get
            {
                Int32 result = -1;
                lock (myLock)
                {
                    if (ep!=null && endpoints.ContainsKey(ep.ToString()))
                        result = endpoints[ep.ToString()];
                }
                return result;
            }
        }

        public ProcessInfo FindProcessByEP(PublicEndPoint ep)
        {
            ProcessInfo result = null;
            string epString = ep.ToString();
            lock (myLock)
            {
                log.DebugFormat("Find {0} in address book", ep.ToString());
                //log.Debug("Address Book Contents:");
                //Dictionary<string, int>.Enumerator iterator = endpoints.GetEnumerator();
                //while (iterator.MoveNext())
                //    log.DebugFormat("{0,-20}{1}", iterator.Current.Key, iterator.Current.Value);

                if (ep != null && endpoints.ContainsKey(ep.ToString()))
                {
                    result = this[endpoints[ep.ToString()]];
                    log.DebugFormat("Found: {0}", result.ToString());
                }
                else
                    log.DebugFormat("{0} not found in address book", ep.ToString());
            }

            return result;
        }

        public List<ProcessInfo> FilterProcesses(ProcessInfo.ProcessType type)
        {
            List<ProcessInfo> result = new List<ProcessInfo>();
            lock (myLock)
            {
                Dictionary<int, ProcessInfo>.Enumerator iterator = processes.GetEnumerator();
                while (iterator.MoveNext())
                    if (iterator.Current.Value.Type == type)
                        result.Add(iterator.Current.Value);
            }
            return result;
        }

        public List<ProcessInfo> FilterProcesses(ProcessInfo.StatusCode status)
        {
            List<ProcessInfo> result = new List<ProcessInfo>();
            lock (myLock)
            {
                Dictionary<int, ProcessInfo>.Enumerator iterator = processes.GetEnumerator();
                while (iterator.MoveNext())
                    if (iterator.Current.Value.Status == status)
                        result.Add(iterator.Current.Value);
            }
            return result;
        }


        public List<ProcessInfo> Processes
        {
            get
            {
                List<ProcessInfo> result;
                lock (myLock)
                {
                    result = processes.Values.ToList();
                }
                return result;
            }
        }

        public void LogContents()
        {
            lock (myLock)
            {
                log.Debug("Process dictionary:");
                Dictionary<Int32, ProcessInfo>.Enumerator iterator = processes.GetEnumerator();
                while (iterator.MoveNext())
                    log.DebugFormat("{0,10} {1}", iterator.Current.Key, iterator.Current.Value.ToString());
            }
        }

    }
}
