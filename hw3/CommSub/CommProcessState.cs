﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using SharedObjects;
using CommSub;
using Utils;

using log4net;


namespace CommSub
{
    public class CommProcessState : State
    {
        #region Private Data Members
        private static readonly ILog log = LogManager.GetLogger(typeof(CommProcessState));

        private CommSubsystem commSubsystem = new CommSubsystem();
        #endregion

        #region Public Process Stuff
        public ProcessInfo MyProcessInfo { get; set; }
        public ProcessInfo RegistryInfo { get; set; }

        public override string StatusString
        {
            get
            {
                return (MyProcessInfo == null) ? base.StatusString : MyProcessInfo.StatusString;
            }
        }

        public void Dispose()
        {
            lock (myLock)
            {
                if (commSubsystem != null)
                    commSubsystem.Stop();
                commSubsystem = null;
            }
        }

        public void ChangeStatus(ProcessInfo.StatusCode newStatus)
        {
            MyProcessInfo.Status = newStatus;
            RaiseStateChangedEvent();
        }

        public void DoShutdown()
        {
            MyProcessInfo.Status = ProcessInfo.StatusCode.Terminating;
            Quit = true;
            RaiseShutdownEvent();
        }

        #endregion
    }
}
