﻿using System;
using System.Net;
using System.Net.Sockets;
using System.IO;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using CommSub;
using Messages.StreamMessages;
using SharedObjects;

namespace CommSubTesting
{
    [TestClass]
    public class NetworkStreamExtensionTester
    {
        [TestMethod]
        public void NetworkStreamExtension_TestEverything()
        {
            TcpListener listener = new TcpListener(new IPEndPoint(IPAddress.Any, 14000));
            listener.Start();

            TcpClient client = new TcpClient();
            client.Connect(new IPEndPoint(IPAddress.Loopback, 14000));
            Assert.IsTrue(client.Connected);
            
            TcpClient server = listener.AcceptTcpClient();
            Assert.IsNotNull(server);

            ProcessInfo p1 = new ProcessInfo() { ProcessId = 2, Label = "Test Process", EndPoint = new PublicEndPoint() { HostAndPort = "127.0.0.1:12340" } };
            InGame msg1 = new InGame() { Process = p1, SeqNr = 1 };

            NetworkStream serverStream = server.GetStream();
            NetworkStream clientStream = client.GetStream();
            clientStream.ReadTimeout = 1000;

            serverStream.WriteStreamMessage(msg1);
            StreamMessage msg2 = clientStream.ReadStreamMessage();
            Assert.IsNotNull(msg2);
            Assert.IsTrue(msg2 is InGame);
            InGame msg3 = msg2 as InGame;
            Assert.AreEqual(msg1.Process.ProcessId, msg3.Process.ProcessId);
            Assert.AreEqual(msg1.Process.Label, msg3.Process.Label);
            Assert.AreEqual(msg1.Process.EndPoint, msg3.Process.EndPoint);
            Assert.AreEqual(msg1.SeqNr, msg3.SeqNr);
        }
    }
}
