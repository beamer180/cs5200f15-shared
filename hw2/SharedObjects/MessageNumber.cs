﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization;

namespace SharedObjects
{
    [DataContract]
    public class MessageNumber
    {
        #region Private Properties
        private static Int32 nextSeqNumber = 0;                     // Start with message #1
        #endregion

        #region Public Properties
        public static Int32 LocalProcessId { get; set; }            // Local process Id -- set once when the
                                                                    // process joins the distributed application
        [DataMember]
        public Int32 ProcessId { get; set; }
        [DataMember]
        public Int32 SeqNumber { get; set; }

        #endregion

        #region Constructors and Factories
        /// <summary>
        /// Default constructor, used in message decoding methods.
        /// </summary>
        public MessageNumber() { }

        /// <summary>
        /// Factory method creates and new, unique message number.
        /// </summary>
        /// <returns>A new message number</returns>
        public static MessageNumber Create()
        {
            MessageNumber result = new MessageNumber()
                {
                    ProcessId = LocalProcessId,
                    SeqNumber = GetNextSeqNumber()
                };
            return result;
        }

        public MessageNumber Clone()
        {
            return this.MemberwiseClone() as MessageNumber;
        }

        // Used for testing
        public static void ResetSeqNumber() { nextSeqNumber = 0; }

        #endregion

        #region Overridden public methods of Object
        public override string ToString()
        {
            return ProcessId.ToString() + "." + SeqNumber.ToString();
        }
        #endregion

        #region Private Methods
        private static Int32 GetNextSeqNumber()
        {
            if (nextSeqNumber == Int32.MaxValue)
                nextSeqNumber = 0;
            return ++nextSeqNumber;
        }
        #endregion

        #region Comparison Methods and Operators
        public override int GetHashCode()
        {
            return (ProcessId << 16) | (SeqNumber & 0xFFFF);
        }

        public override bool Equals(object obj)
        {
            return Compare(this, obj as MessageNumber) == 0;
        }

        public static int Compare(MessageNumber a, MessageNumber b)
        {
            int result = 0;

            if (!System.Object.ReferenceEquals(a, b))
            {
                if (((object)a == null) && ((object)b != null))
                    result = -1;                             
                else if (((object)a != null) && ((object)b == null))
                    result = 1;                             
                else
                {
                    if (a.ProcessId < b.ProcessId)
                        result = -1;
                    else if (a.ProcessId > b.ProcessId)
                        result = 1;
                    else if (a.SeqNumber < b.SeqNumber)
                        result = -1;
                    else if (a.SeqNumber > b.SeqNumber)
                        result = 1;
                }
            }
            return result;
        }

        public static bool operator ==(MessageNumber a, MessageNumber b)
        {
            return (Compare(a,b) == 0);
        }

        public static bool operator !=(MessageNumber a, MessageNumber b)
        {
            return (Compare(a,b) !=0 );
        }

        public static bool operator <(MessageNumber a, MessageNumber b)
        {
            return (Compare(a, b) < 0);
        }

        public static bool operator >(MessageNumber a, MessageNumber b)
        {
            return (Compare(a, b) > 0);
        }

        public static bool operator <=(MessageNumber a, MessageNumber b)
        {
            return (Compare(a, b) <= 0);
        }

        public static bool operator >= (MessageNumber a, MessageNumber b)
        {
            return (Compare(a, b) >= 0);
        }

        #endregion

        /// <summary>
        /// This is a class provide a comparer so MessageNumber can used as a dictionary key
        /// </summary>
        public class MessageNumberComparer : IEqualityComparer<MessageNumber>
        {
            public bool Equals(MessageNumber msgNumber1, MessageNumber msgNumber2)
            {
                return msgNumber1 == msgNumber2;
            }

            public int GetHashCode(MessageNumber msgNr)
            {
                return msgNr.GetHashCode();
            }
        }
    }


}
