﻿using System;
using System.Runtime.Serialization;

using SharedObjects;

namespace Messages
{
    [DataContract]
    public class LoginReply : Reply
    {
        [DataMember]
        public ProcessInfo ProcessInfo { get; set; }
    }
}
